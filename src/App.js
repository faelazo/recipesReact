import React, { Component } from 'react';
import './App.css';
import Content from './components/Content'
import Header from './components/Header';

class App extends Component {

      constructor(){
            super();
            this.state = {
                  panel: "home"
            };
      };

      seleccionado = opcion => {
            this.setState({panel: opcion})
      }

      render() {
            return (
                  <div className="App">
                        <Header seleccion={this.seleccionado}></Header>
                        <Content panel={this.state.panel}></Content>
                  </div>
            );
      };
}

export default App;
