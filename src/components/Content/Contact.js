import React from 'react';

const Contact = () => {

      return (
            <div id="divContacto">
                  <h1>Contacto</h1>
                  <p>Rellene el siguiente formulario indicando su nombre, su correo electrónico e indíquenos su sugerencia. Le responderemos lo antes posible.</p>
                  <p>Muchas gracias por usar Recetas React</p>
                  <form action="" method="GET">
                        <label htmlFor='name'>Nombre:</label>
                        <br />
                        <input type="text" name="name" className="fildForm"/>
                        <br />
                        <label htmlFor='email' >eMail:</label>
                        <br />
                        <input type="email" name="email" className="fildForm"/>
                        <br />
                        <label htmlFor='description' >Sugerencia:</label>
                        <br />
                        <textarea name="description" className="fildForm"></textarea>
                        <br />
                        <input type="submit" value="Enviar" />
                  </form>
            </div>
      );
};

export default Contact;
