import React from 'react';
import './styles.css';

const Recipe = ({titulo, imagen, calorias, duracion, salud}) => {


      return (
            <div id="divReceta">
                  <img id="fotoReceta" src={imagen} alt="Foto Receta" />
                  <h1 id="titReceta">{titulo}</h1>
                  <div id="propsReceta">
                        <table>
                              <tbody>
                                    <tr>
                                          <td className="titProp">Categoría:</td>
                                          <td className="valProp">{salud}</td>
                                    </tr>
                                    <tr>
                                          <td className="titProp">Calorías:</td>
                                          <td className="valProp">{calorias.toFixed(2)}</td>
                                    </tr>
                                    <tr>
                                          <td className="titProp">Duración:</td>
                                          <td className="valProp">{duracion}</td>
                                    </tr>
                              </tbody>
                        </table>
                  </div>
            </div>
      );
};



export default Recipe;
