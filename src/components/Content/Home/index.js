import React from 'react'
import Receta from './Recipe';


const Home = ({recetas}) => {

      const montarRecetas = () => {
            return recetas.map( receta => {
                  return (<Receta   key={receta.recipe.label}
                                    className="fichaReceta"
                                    titulo={receta.recipe.label}
                                    imagen={receta.recipe.image}
                                    salud={receta.recipe.healthLabels[0]}
                                    calorias={receta.recipe.calories}
                                    duracion={receta.recipe.totalTime} ></Receta>)
            });
      };

      return (
            <div id="listaRecetas">
                  {montarRecetas()}
            </div>
      );
};

export default Home;
