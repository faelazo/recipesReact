import React from 'react';
import './styles.css';
import Home from './Home';
import Contact from './Contact';
import About from './About';

class Content extends React.Component{

      constructor(){
            super();
            this.state = {
                  recetas: []
            }
      }

      componentWillMount(){
            var prop_fetch = {
                  method: 'GET',
                  dataType: 'json'
            };

            fetch('https://api.edamam.com/search?q=sugar&app_id=XXXXXXd&app_key=XXXXXXXXXXXXXXX&from=0&to=3&calories=591-722&health=alcohol-free', prop_fetch).then(data => {
                        return data.json();
                  }).then( data => {
                        this.setState({recetas: data.hits});
                  });
      }

      getContent = () => {
            switch (this.props.panel) {
                  case 'about':
                        return (<About id="about"></About>);
                  case 'contact':
                        return (<Contact id="contacto"></Contact>);
                  default:
                        return (<Home id="recetas" recetas={this.state.recetas}></Home>);
            }
      };

      render() {

            return (
                  <div id="contenido">
                        {this.getContent()}
                  </div>
            );
      };
};

export default Content;
