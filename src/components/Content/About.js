import React from 'react';

const About = () =>{

      return (
            <div id="divAbout">
                  <h1 id="titAbout">Conócenos</h1>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque non maximus nulla. Curabitur malesuada nulla neque, in porta risus pretium vel. Aenean congue dignissim quam eget mattis. Aliquam tincidunt urna et nisi dignissim pulvinar. Nunc nulla sem, scelerisque vitae tortor sed, pulvinar ultrices nibh. Morbi dolor elit, tempus ac risus sed, porta interdum tellus. Duis vitae ante vitae velit porttitor malesuada. Suspendisse molestie pellentesque nulla ac mollis. Curabitur ut ultricies diam.</p>
                  <p>Donec justo mauris, varius vitae nisi et, sodales efficitur dui. Nunc non faucibus odio. In vulputate facilisis arcu non pharetra. In hac habitasse platea dictumst. Integer tristique, magna eget sollicitudin viverra, sem turpis mollis magna, sit amet mattis sem sapien porta justo. Ut volutpat malesuada tellus et hendrerit. Vestibulum sollicitudin et nibh vitae malesuada. In aliquet, elit eget placerat congue, lorem urna tincidunt nisl, in varius metus nibh at ex. Sed viverra quis libero sit amet efficitur. Ut vitae tortor risus. Sed bibendum diam accumsan neque sagittis, ut tincidunt ex ullamcorper. Etiam sit amet sagittis tellus. Maecenas nec lacinia lacus.</p>
                  <p>In a augue et ligula pretium sollicitudin eget posuere urna. Suspendisse potenti. Aliquam pretium est turpis, volutpat condimentum leo pretium quis. Mauris aliquet commodo fermentum. Etiam eget convallis metus, sed rhoncus orci. Sed quis ipsum mi. Nunc vel venenatis arcu. Duis iaculis tortor elit, eget aliquam diam dictum ut. Maecenas sit amet purus turpis. Donec quis erat ac leo feugiat aliquam. Suspendisse molestie risus finibus lorem interdum, ut maximus tellus pulvinar.</p>
                  <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla porttitor lorem risus, eleifend rhoncus arcu ullamcorper ut. Cras convallis gravida mi, at sodales nulla imperdiet vitae. Donec ornare lacus arcu, vel placerat tellus facilisis quis. Vivamus tempor pharetra tortor ac dignissim. Aliquam pretium magna eu elementum tincidunt. Cras vehicula hendrerit metus vel mollis. Nam feugiat suscipit justo, ut porta magna suscipit eu.</p>
                  <p>Sed purus odio, accumsan vel libero vitae, viverra dapibus neque. In sit amet elit eu tellus sollicitudin efficitur ac eget tellus. In hac habitasse platea dictumst. Sed ac nisl quis sapien egestas rutrum ut lobortis libero. Vivamus ut turpis et tortor bibendum faucibus. Integer eleifend molestie feugiat. Pellentesque consectetur efficitur ligula in facilisis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Praesent et convallis enim. Curabitur tristique mauris in eros placerat suscipit. Sed eu vehicula metus. Sed tristique odio eget est iaculis venenatis. Nunc luctus vestibulum pellentesque.</p>
            </div>
      );
};

export default About;
