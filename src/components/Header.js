import React from 'react';
import logo from './../img/book.png';
import './styles.css';

const Header = ({seleccion}) => {

      return (
            <header>
                  <div id="divLogo">
                        <img id="logo" src={logo} alt="RecipeBook"/>
                        <h1 id="nombreWeb">RECETAS REACT</h1>
                  </div>
                  <div id="divMenu">
                        <nav id="menu">
                              <ul>
                                    <li onClick={() => seleccion('home')} value="home">INICIO</li>
                                    <li onClick={() => seleccion('contact')} name="contact">CONTACTO</li>
                                    <li onClick={() => seleccion('about')} name="about">CONÓCENOS</li>
                              </ul>
                        </nav>
                  </div>
            </header>
      );
};

export default Header;
